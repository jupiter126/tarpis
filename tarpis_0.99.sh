#!/bin/bash
#
# Tarpis (Time and relative pictures in Space)
# Little script to help me sort and manage the clusterfuck of pictures I got back from the cloud.
###
# MIT LICENCE in readme - Copyright 2020 -> tomorrow jupiter126@gmail.com
######## Var Declaration
source polyconf.sh.php
######## Init
def=$(tput sgr0);bol=$(tput bold);red=$(tput setaf 1;tput bold);gre=$(tput setaf 2;tput bold);yel=$(tput setaf 3;tput bold);blu=$(tput setaf 4;tput bold);mag=$(tput setaf 5;tput bold);cya=$(tput setaf 6;tput bold) #colors
ddirectory="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#######  Cleaning of filesystem hierarchy
function f_cleaning {
	detox -r "$ddirectory"	# 1. Recursively clean all file and folder names from toxic characters
	for ffile in $(find ~+ -type f -name "*.json"); do rm "$ffile";	done # 2. Remove any json file - we don't use them
	find . -name '*.*' | grep '\.[A-Z][A-Z]*$'| while IFS= read -r f # 3. Extensions to lowercase - just in case
	do
		a=$(echo "$f" | sed -r "s/([^.]*)\$/\L\1/");
		[ "$a" != "$f" ] && mv "$f" "$a"
	done
	for dddir in $(find ~+ -type d -empty); do rmdir "$dddir"; done #4. remove all empty directories
}

#######  Pic Geolocation - locmethod point to different API providers
function f_locmethod_1 {
	llocation=($(curl -s "https://nominatim.openstreetmap.org/reverse?format=xml&accept-language=en&lat=${eexifdata[0]}&lon=${eexifdata[1]}&zoom=5"))
}
function f_locmethod_2 {
	llocation=($(curl -s "https://eu1.locationiq.com/v1/reverse.php?key=$locationiq&format=xml&lat=${eexifdata[0]}&lon=${eexifdata[1]}&zoom=5"))
}
function f_locmethod_3 {
	llocation=($(curl -s "https://api.opencagedata.com/geocode/v1/xml?q=${eexifdata[0]}+${eexifdata[1]}&key=$opencage"))
}
function f_locmethod_4 {
	 f_locmethod_2 # because API2 has twice the limit of API1 and API3
}

apiloop="0";apimethod="1"
function f_geotagAPI { # converts ${eexifdata[0]} and ${eexifdata[1]} into ${country[@]} and ${region[@]}
		if [[ "$apiloop" -gt "$maxloop" ]]; then sleeptime="$((86400-$maxloop))" && echo "$red Daily geotag query limit reached, sleeping for $sleeptime seconds $def" && sleep $sleeptime; fi
		ccountry="" && rregion="" # we reset to not keep old results in case of empty answer
		f_locmethod_$apimethod # looping through free API providers to avoid getting booted
		ccountry=($(echo "${llocation[@]}"|grep -oP '(?<=country>)[^<]+'|sed -e 's/ /-/g'))
		rregion=($(echo "${llocation[@]}"|grep -oP '(?<=state>)[^<]+'|sed -e 's/ /-/g'))
		if [[ "$vverbal" -ge 3 ]]; then echo "locmethod $apimethod gave us back $ccountry $rregion"; fi
		((apimethod=apimethod+1)) && ((apiloop=apiloop+1))
		if [[ "$apimethod" -gt "4" ]]; then apimethod="1" ; fi
		sleep 1 # to avoid being kicked from free api
}

function f_mmediatype {
	ffil="$1"
	eext=$(echo "$1"|rev|cut -f1 -d'.'|rev)
	if [[ "$(echo $imgfile|grep $eext)" != "" ]]; then
		mmediatype="img"
	elif [[ "$(echo $vidfile|grep $eext)" != "" ]]; then
		mmediatype="vid"
	else
		mmediatype="unknown"
	fi
}

function f_getexifdata {
	eexifdata=($(exiftool -f -n -p '$GPSLatitude,$GPSLongitude,$CreateDate,$ModifyDate' $mmedia 2>/dev/null|sed -e 's/,/ /g' -e 's!/!:!g')) #get exif data, stored as lat -> ${eexifdata[0]} lon -> ${eexifdata[1]} createdate -> ${eexifdata[2]} createtime -> ${eexifdata[3]} moddate -> ${eexifdata[4]} modtime -> ${eexifdata[5]}.
	if [[ "$vverbal" -ge "3" ]] ; then echo "${eexifdata[@]}" - "${eexifdata[0]}" - "${eexifdata[1]}" - "${eexifdata[2]}" - "${eexifdata[3]}" - "${eexifdata[4]}" - "${eexifdata[5]}";fi
	if  [[ ! $(date -d $(echo "${eexifdata[2]}"|sed -e 's!:!/!g')) ]] &>/dev/null || [[ ! "${eexifdata[2]}" =~ ^[0-9]{4}:[0-9]{2}:[0-9]{2}$ ]] ; then # if date format is not conform
		eexifdata[2]="${eexifdata[3]}";eexifdata[3]="${eexifdata[4]}"; # then swap one position (not two) because if exif didnt have createtime, then fields 2 and 3 are grouped as blank 2 - hence we swap 3 and 4 to 2 and 3
		if  [[ ! $(date -d $(echo "${eexifdata[2]}"|sed -e 's!:!/!g')) ]] &>/dev/null || [[ ! "${eexifdata[2]}" =~ ^[0-9]{4}:[0-9]{2}:[0-9]{2}$ ]] ; then # if still not conform
			eexifdata[2]="$(date +%Y:%m:%d)" # set current date
			eexifdata[3]="$(date +%H:%M:%S)" # set current time
		fi
	fi
	if [[ "$vverbal" -ge "3" ]]; then echo "$mmedia : ${eexifdata[@]}"; fi
}

function f_setnewdest { # defines new destination ($newdest); adds $filenamedate if $mmedia does not contain it
	if [[ "x$(echo $mmedia|grep $ffilenamedate)" = "x" ]]; then # if $mmedia contains $filenamedate, don't add it, else, add it
		newname="$(echo $shortname-$ffilenamedate.$eextension | sed -e 's/-\././g' -e 's/--/-/')"
	else
		newname="$(echo $shortname.$eextension | sed -e 's/-\././g' -e 's/--/-/')"
	fi
	newdest="$ddirectory/$yyear/$mmonth/$newname"
}

function f_keepthebiggest { # keeps the biggest file in case of name conflict
	if [[ -f "$newdest" ]]; then # If target file already exists, then keep the biggest one.
		if [[ "x$mmedia" != "x$newdest" ]]; then
			dstsize="Z" && srcsize="A"
			srcsize=$(stat -c %s "$mmedia")
			dstsize=$(stat -c %s "$newdest")
			if [[ "$dstsize" -lt "$srcsize" ]]; then mkdir -p "$ddirectory/$yyear/$mmonth/" && mv "$mmedia" "$newdest"; if [[ "$vverbal" -ge "3" ]]; then echo "$red Destination is smaller --> Replacing with source $def" && return 0; fi
			elif [[ "$dstsize" -ge "$srcsize" ]]; then if [[ "$vverbal" -ge "3" ]]; then echo "$red Destination is BIGGER or equal --> removing source $def"; fi && rm "$mmedia" && return 1
			fi;
		else
			if [[ "$vverbal" -ge "3" ]]; then echo "media and newdest are the same"; fi
			return 1
		fi
	else
		mkdir -p "$ddirectory/$yyear/$mmonth/"
		mv "$mmedia" "$newdest" && return 0 # file does not exist --> moving to dest
	fi
}

function f_checksumcheck {
	md5chk="$(md5sum $newdest|cut -f1 -d' ')"
	results2=()
	query="select filename from pictures where md5chk='$md5chk';"
	results2=($(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query"))
	if [[ "x${results2[@]}" != "x" ]]; then #Avoid duplicate files
		for thatdupe in ${results2[@]}; do
			if [[ $(sha256sum "$newdest"|cut -f1 -d' ') = $(sha256sum "$thatdupe"|cut -f1 -d' ') ]]; then
				rm "$newdest" && return 1
			fi
		done
	fi; return 0
}

function f_fileisindb { # returns 1 if a filename is in the db, 0 if not
	rresults=""
	query="select filename from pictures where filename='$mmedia';"
	rresults="$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")"
	if [[ "x$rresults" = "x" ]]; then # Only proceed if file is not allready in db
		return 0;
	else
		return 1;
	fi
}

function f_writedb {
	query="select id from pictures where filename='$newdest';"
	rresults=$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")
	if [[ "$rresults" = "" ]]; then # if $newdest has no record in the database, we create one
		echo "use $sqldb;INSERT INTO pictures (md5chk, filename, latitude, longitude, region, country, genesis) VALUES ('$md5chk', '$newdest', '${eexifdata[0]}', '${eexifdata[1]}', '${rregion[@]}', '${ccountry[@]}', '$dbdate');" |mysql -u $sqluser -p$sqlpass
	else # else we update the existing record
		echo "use $sqldb;update pictures set md5chk='$md5chk', latitude='${eexifdata[0]}', longitude='${eexifdata[1]}', region='${rregion[@]}', country='${ccountry[@]}', genesis='$dbdate' where filename='$newdest';"|mysql -u $sqluser -p$sqlpass
	fi
}

function f_order_dedupe_index { # Parses and sorts all files to add them to the DB
	if [[ "$vverbal" -ge "1" ]]; then echo "$yel Patience, generating database. . .$red this takes a while, go to sleep! $def"; indexloop=0; fi
	for eextension in $imgfile $vidfile; do #feel free to add more extensions to parse!
		for mmedia in $(find ~+ -type f -name "*.$eextension"); do #for each file we find of that extension
			if [[ "$(echo $mmedia|rev|cut -f1 -d'/'|rev)" = "tarpis.jpg" ]]; then continue; fi # dont move our logo!
			if [[ "$vverbal" -ge "1" ]]; then ((indexloop=indexloop+1)); if [[ "x$(echo "$indexloop"|grep 000$)" != "x" ]]; then echo "$yel $indexloop records indexed. APIcount: $apiloop$def"; fi; fi
			f_fileisindb || continue
			newdest=""
			f_getexifdata
			dbdate="$(echo "${eexifdata[2]} ${eexifdata[3]}"|cut -f1 -d'+') " # concatenate string compatible with sql datetime (and remove eventual +01:00)
			ffilenamedate="$(echo "$dbdate"|sed -e 's/://g' -e 's/ /-/g' -e 's/-$//' )" # format compatible with filesystem
			yyear="$(echo ${eexifdata[2]}|cut -f1 -d:)"
			mmonth="$(echo ${eexifdata[2]}|cut -f2 -d:)"
			shortname="$(echo $mmedia| rev | cut -d'/' -f1 | cut -f 2- -d '.'| rev)" # filename withouth path or extension
			f_setnewdest
			f_keepthebiggest || continue
			f_checksumcheck || continue
			if [[ "${eexifdata[0]}" = "-" ]] || [[ "${eexifdata[1]}" = "-" ]]; then # geotag comes in last position to avoid unnessecary API calls
				rregion="Unknown" && ccountry="Unknown"
			else
				f_geotagAPI
			fi
			f_writedb
			if [[ "$vverbal" -ge "2" ]]; then echo "$red $newdest has been indexed $def"; fi
		done
	done
	query="SELECT MAX(id) FROM pictures;"
	recid="$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")"
	if [[ "$vverbal" -ge "1" ]]; then echo "$red Everything is indexed, thanks for waiting ! $mag $recid $gre elements in database.$def"; fi
}

function f_initpage { # initiates page header
	cat "tarpis_files/2_complete_header.template" > "$ppage.html"
	echo "<h2>Welcome to $ppage</h2>" >> "$ppage.html"
	cat "tarpis_files/5_body_template_2" >> "$ppage.html"
}

function f_tarpis { #generate html gallery
	if [[ "$vverbal" -ge "1" ]]; then echo "$gre Generating tarpis to travel accross space and time. $def"; fi
	### create space array
	spacenavbar=()
	query="SELECT DISTINCT Country FROM pictures where Country!='Unknown' and Country!=' ' order by country asc;"
	ccountrylist=($(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query"))
	for ccountry in ${ccountrylist[@]}; do spacenavbar+=("<li><a href=\"$ccountry.html\">$ccountry</a></li>"); done
	### create time array
	timenavbar=()
	query="SELECT DISTINCT YEAR(genesis) FROM pictures where YEAR(genesis)!='0' order by YEAR(genesis) asc;"
	yyearlist=($(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query"))
        for yyear in ${yyearlist[@]}; do timenavbar+=("<li><a href=\"$yyear.html\">$yyear</a></li>"); done
	# Build tarpis menu based on space and time arrays
	cat "tarpis_files/1_header.template_1" > "tarpis_files/2_complete_header.template"
	echo "${timenavbar[@]}" >> "tarpis_files/2_complete_header.template"
	cat "tarpis_files/1_header.template_3" >> "tarpis_files/2_complete_header.template"
	echo "${spacenavbar[@]}" >> "tarpis_files/2_complete_header.template"
	cat "tarpis_files/1_header.template_5" >> "tarpis_files/2_complete_header.template"
	#initiate index.html space.html and time.html
	cat "tarpis_files/2_complete_header.template" | tee "time.html" | tee "space.html" > index.html;
	echo "<h2><strong>Welcome to the tarpis</strong></h2><p>Travelling trouhg time and space since when it will eventually work.</p>">>index.html
	echo "<h2><strong>Welcome to Space</strong></h2><p>Only geotagged pictures are in part of this gallery</p>">>space.html
	echo "<h2><strong>Welcome to Time</strong></h2><p>All Pictures are in this gallery</p>">>time.html
	cat "tarpis_files/5_body_template_2" | tee -a "time.html" | tee -a "space.html" >> index.html;
	echo "<div class=row>" | tee -a "time.html" | tee -a "space.html" >> index.html;
	echo "<div class='center col s6'><div class=\"card small\"><a href=space.html> SPACE</a><div class=\"card-content\"><a href=space.html><h4>Space</h4></a></div></div></div>" >> "index.html"
	echo "<div class='center col s6'><div class=\"card small\"><a href=time.html> TIME</a><div class=\"card-content\"><a href=time.html><h4>Time</h4></a></div></div></div>" >> "index.html"
	echo "</div>" >> index.html
	### Fill space and time
	for level0 in ${ccountrylist[@]} ${yyearlist[@]}; do
		[[ "$level0" == ?(-)+([0-9]) ]] && ttype="time" || ttype="space"
		ppage="$level0" && f_initpage
		echo "<div class=\"container\"> <h3>$level0</h3> <div class=\"row card\">" >> "$level0.html"
		if [[ "$ttype" = "space" ]]; then  query="select filename from pictures where country='$level0' limit 1;"
		elif [[ "$ttype" = "time" ]]; then query="select filename from pictures where YEAR(genesis)='$level0' limit 1;"
		fi
		level0pic=$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")
		f_mmediatype $level0pic
		if [[ "$mmediatype" = "img" ]]; then
			echo "<div class='center col s3'><div class=\"card small\"><div class=\"card-image\"><a href=$level0.html><img src=\"$level0pic\"></a></div><div class=\"card-content\"><a href=$level0.html><h4>$level0</h4></a></div></div></div>" >> "$ttype.html"
		elif [[ "$mmediatype" = "vid" ]]; then
#############################still miss code here
			echo "<div class='center col s3'><div class=\"card small\"><div class=\"card-image\"><a href=$level0.html><img src=\"$level0pic\"></a></div><div class=\"card-content\"><a href=$level0.html><h4>$level0</h4></a></div></div></div>" >> "$ttype.html"
		fi
		if [[ "$ttype" = "space" ]]; then  query="SELECT DISTINCT region FROM pictures where Country='$level0' and region!=' ';"
		elif [[ "$ttype" = "time" ]]; then query="SELECT DISTINCT MONTHNAME(genesis) FROM pictures where YEAR(genesis)='$level0' order by Month(genesis) asc;"
		fi
		level1list=$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")
		for level1 in ${level1list[@]}; do
			ppage="$level0-$level1" && f_initpage
			echo "<div class=\"container\"> <h3>$level1</h3> <div class=\"row card\">" >> "$level0-$level1.html"
			if [[ "$ttype" = "space" ]]; then  query="select filename from pictures where region='$level1' limit 1;"
			elif [[ "$ttype" = "time" ]]; then query="select filename from pictures where YEAR(genesis)='$level0' and monthname(genesis)='$level1' limit 1;"
			fi
			level1pic=$(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query")
			echo "<div class='center col s3'><div class=\"card small\"><div class=\"card-image\"><a href=$level0-$level1.html><img src=\"$level1pic\"></a></div><div class=\"card-content\"><a href=$level0-$level1.html><h5>$level1</h5></a></div></div></div>" >> "$level0.html"
			if [[ "$ttype" = "space" ]]; then  query="select filename from pictures where region='$level1' order by genesis asc;"
			elif [[ "$ttype" = "time" ]]; then query="select filename from pictures where YEAR(genesis)='$level0' and monthname(genesis)='$level1' order by genesis asc;"
			fi
			level2pics=($(mysql -u $sqluser -p$sqlpass $sqldb -N -B -r -e "$query"))
			for level2pic in ${level2pics[@]}; do
#				echo "<div class=\"col s12 m6 l4\"><img class=\"materialboxed\" vspace=\"5\" width=\"275\" height=\"150\" src=\"$level1pic\" class=\"materialbox responsive-img card\"></div>" >> "$level0-$level1.html" # aligns but distorts
				echo "<div class=\"col s12 m6 l4\"><img class=\"materialboxed\" vspace=\"5\" width=\"275\" src=\"$level2pic\" class=\"materialbox responsive-img card\"></div>" >> "$level0-$level1.html" # does not align but does not distort:
			done
			echo "</div> </div>" >> "$level0-$level1.html"
			cat "tarpis_files/9_footer.template" >> "$level0-$level1.html"
		done
		echo "</div> </div>" >> "$level0.html"
		cat "tarpis_files/9_footer.template" >> "$level0.html"
	done
	### Close Universe
	echo "</div>" | tee -a "time.html" >> "space.html";
	cat "tarpis_files/9_footer.template" | tee -a "time.html" | tee -a "space.html" >> index.html;
}

function f_resetdb {
		echo "use $sqldb;truncate table pictures;"|mysql -u $sqluser -p$sqlpass  #was used to reset db
}

function m_main { # Main Menu (displayed if genQL is called without args)
while [ 1 ]
do
	PS3='Choose a number: '
	select choix in "Clean Filesystem" "Index" "Generate Gallery" "Reset Database" "Quit"
	do
		echo " ";echo "####################################";echo " "
		break
	done
	case $choix in
		"Clean Filesystem")	f_cleaning ;;
		"Index")		f_order_dedupe_index ;;
		"Generate Gallery")	f_tarpis ;;
		"Reset Database")	f_resetdb ;;
		Quit)			echo "bye ;)";exit ;;
		*)			echo "$mag Same player shoot again!$def" ;;
	esac
done
}

#Main entry point: Displays menu if no arguments passed, displays help if wrong argument passed
if [[ "x$1" = "x" ]]; then
	m_main
elif [[ "x$1" = "xclean" ]]; then
	f_cleaning
elif [[ "x$1" = "xindex" ]]; then
	f_order_dedupe_index
elif [[ "x$1" = "xtarpis" ]]; then
	f_tarpis
elif [[ "x$1" = "xresetdb" ]]; then
	f_resetdb
elif [[ "x$1" = "xall" ]]; then
	f_resetdb; f_cleaning; f_order_dedupe_index; f_tarpis
else
	echo "$blu Optionnal parameters are$yel
	- No parameter for main menu
	- clean: Clean file hierarchy structure
	- index: Add all non-indexed images to the database
	- resetdb: resets the database
	- tarpis: Generate your static gallery to travel through time and space
	- all: all of the above$def"
fi
