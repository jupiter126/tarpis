# README #

When Google announced that I'd have to do something about my pictures, I first downloaded all my pics, and then couldn't find a suitable alternative.
Hence I started to make my own...

Tarpis is a bash script that allows to view pictures, traveling in time and space.
It works by reordering and indexing all files on a YYYY/MM/ basis, eleminitaing duplicates and then generating a static website alloing to browse them based on exif data (time and space).

I was originally planning for another name, but as the BBC didn't reply my email I decided to avoid problems.  Hence the tarpis got both it's appearance and a letter stuck in an emergency while traveling trough time and space.

### !!! Use with caution: the tarpis will move ALL $eextension files in current dir recursively.  It is strongly advised you run it in /home/youruser/pictures/ rather than /home/youruser - or even worse from /.
1. Backup your files in case you do not like what the tarpis does (changes in space and time are irreversible unless you have a backup)
2. Copy the script and its folder in the folder where you want to reorganise the media files and generate the gallery
3. Run the script like described in the next section.

### Requirements:
 - rename detox exiftool (apt install rename detox exiftool)
 - mysql/mariadb with the following setup (replace adequate variables):
	- create database $sqldb;
	- use $sqldb;
	- CREATE TABLE pictures (id int(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY, md5chk VARCHAR(35), filename VARCHAR(300) unique,latitude varchar(20),longitude varchar(20), region varchar(30), country varchar(30), genesis datetime);
	- CREATE USER '$sqluser'@'localhost' IDENTIFIED BY '$sqlpass';
	- GRANT ALL PRIVILEGES ON $sqldb.pictures TO '$sqluser'@'localhost'; flush privileges;

### Objectives and technical choices.
 - I needed to remove duplicates of my 25k+ unsorted images, hence the cleaning section
 - I wanted a portable gallery (aka can copy it to an usb stick and pass it around) hence the choice of static html
 - I first used an array to check for dupes, but projected running time was over 4 months, so I switched to mysql for this process.
	- Note that mysql is only used for generation of the static site.
	- It feels stupid to rely on mysql for a temporary process but my code was slower by a factor of over 500!

### Configuration(s):
 - The configuration file is polyconf.sh.php.  It is a polyglot script that runs in bash as well as php, which allows for a unique configuration file.
 - Do not forget to define all variables like API keys and db access.  If you only use one API, comment methods accordingly!
 - from php, conf should be called within html comments!
	
### Use:
./tarpis.sh		    Interactive mode (main menu)
./tarpis.sh clean	Clean file hierarchy structure
./tarpis.sh index	Add all non-indexed images to the database - remove dupes - keep the biggest!
./tarpis.sh resetdb	resets the database (mostly used when debugging)
./tarpis.sh tarpis	Generate your static gallery to travel through time and space
./tarpis.sh all		All of the aboce, at once

### GeoAPI methods:
Get your API keys or it won't work.  The three providers I used have free plans!

In order to match exif GPS data to locations, I used some free public API.  I had too much pictures to submit them all at once to one provider, but not enough to justify purchasing a plan, hence I setup a loop to switch between providers.

If you have many pictures and don't want to be limited by API speed, consider a paying plan, and commenting some methods in the script.  Doing so the indexing will be considerably faster as you will be able to remove the "sleep 1" instruction that I set to avoid getting booted from free API.  Also it is thanks to paying customers that those providers can provides API calls for free!

I found three "compatible" Geolocation providers (compatible in terms of API response parsing).
-  https://nominatim.openstreetmap.org/
-  https://eu1.locationiq.com/
-  https://api.opencagedata.com/


### Version history: 
0.1   - works with an array

0.2   - array replaced with mysql = speedup over 500x

0.3   - got base for geolocation (not done yet)

0.4   - making database permanent

0.5   - put db info into variables
      - rework geoloc, switch from json to xml for easier parsing

0.6   - control entry point
      - put everything in functions

0.7   - solved extension rename ressource hog
      - Finished menu
      - Got to a chicken and egg

0.8   - Complete refactoring of the code to reorganise on exif data while indexing
      - better cleaning (detox)
      - Add fields to db

0.9   - Gallery generation prototype \o/

0.91  - 0.9x - Think you got it right? rename functions and variables to what they actually do, optimise and refactorise code, debug and test, update comments to make them relevant, remove the unnecesary; restart iteration!
      - I seem to get the codebase complete and functioning at 0.97

0.99  - Public Release: it kinda worked for my files - few bugs still lurking - can still be optimised/cleaned a bit.

### todo:
- Find why sometimes some files are not found.
- Determine which is the right code to put images in the gallery
- Include Pagination to make pages lighter?
- Generate php files if want to avoid generating static html gallery

I though I was gonna do those, but didn't get the time to finish... I'd rather post the code as is before it gets lost!

### Like it? Send some crypto ^^
Bitcoin	  16oiYYkbf7qFQSk2VuhPsSJJ2r8NT2jbGe

Ethereum  0xB1DcBDe40202b4d5BD352041126Ba6f29f7f4b77

XRP	  rpJYpQoeS1pZVpGpRfHxVggqj37Z4EjBG4

XMR	  87mEcsJMnHALLeWv2mXVan32MFiHDbzC22F4zWuUwNy8i2GrEDSNBwd1W8zSv2oMykZ7rtn6Ne5V5RzZgow7bCdtUyw2mFr

ZCASH	  zs1e390dmtgggd5qq3rkxtaagjqnmv8t9e3lphpaevje54pn5ux7rpyy2urt40zwcsxprtx5k6mjsv

IOTA	  LMCUSBYIEUSMDKGSAARLNTREFZKKAOVNOBOFNSGMKDYNUBVQARKWDRXYDVABKOCEJLLPSOIVYIKQJQKCDCMSHPRSHY
